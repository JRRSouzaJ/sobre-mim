<h1 align="center">Lista de Links</h1>
<h3 align="center">Site simples para agrupar todos os meus perfis em redes sociais em um só lugar.</h3>

<p align="center">
  <a href="#sobre-o-projeto">Sobre o projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#começando">Começando</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#como-contribuir">Como contribuir</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#licença">Licença</a>
</p>

## 💡 Sobre o projeto

Este é um projeto de código aberto que serve como uma alternativa gratuita ao website 
- [Demonstração](https://jrrsouzaj.codeberg.page/sobre-mim/@main/)

## 🚀 Começando

Se você instalou o git você pode clonar o código para sua máquina, ou baixar um ZIP de todos os arquivos diretamente.
[Faça o download do ZIP deste local](https://codeberg.org/JRRSouzaJ/sobre-mim/archive/main.zip), ou execute o seguinte comando [git](https://git-scm.com/downloads) para clonar os arquivos em sua máquina:
```bash
https://codeberg.org/JRRSouzaJ/sobre-mim.git
```
- Assim que os arquivos estiverem em sua máquina, abra a pasta "sobre-mim" no Visual Studio Code. [Visual Studio Code](https://code.visualstudio.com/).
- Com os arquivos abertos em Visual Studio Code, pressione o botão *Go Live* na parte inferior da janela para iniciar os arquivos com o [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer).
- Mude a imagem do perfil e os textos no arquivo `index.html`.
- Mude as cores e fontes no arquivo `style.css`.
- Para mudar o fundo, vá para o arquivo `style.css` na linha 17, descomente o trecho do código e mude a url para a imagem que quiser.

## 🤔 Como contribuir

- Faça um fork deste repositório;
- Crie uma branch com seus recursos: `git checkout -b my-feature`;
- Confirme suas mudanças: `git commit -m "feat: my new feature"`;
- Envie para sua branch: `git push origem my-feature`.

Uma vez que seu pedido de pull tenha sido fundido (merge), você pode apagar sua branch.

## 📝 Licença

Este projeto está sob a licença do MIT. Veja o arquivo [LICENSE](LICENSE.md) para mais detalhes.

---

Este projeto é um fork do originla de John Emerson você pode acessar o original [aqui](https://johnggli.github.io/linktree)
